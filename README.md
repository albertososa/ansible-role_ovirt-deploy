# Ansible role: ovirt-deploy

Create VMs in a oVirt infrastructure.

## Requirements

None.

## Role variables

Available variables are listed below, along with the default values (see `defaults/main.yml`):

```yaml
ovirt_url: https://ovirt.example.org/ovirt-engine/api
```

The URL of the oVirt API to use.

```yaml
ovirt_login:
  username: user@realm
  password: Pa$sw0rd!
```

Set the credentials to connect oVirt API. The username should specify the realm.

```yaml
ovirt_vms: []
```

Set a list with the specification of the VMs to deploy in the oVirt infrastructure. For example:

```yaml
ovirt_vms:
  - name: VM-example-1
    template: centos-7
    cluster: Default
    cpu_cores: 4
    cpu_sockets: 1
    memory: 4GiB
    nics:
      - name: nic1
        profile_name: DOC1
  - name: VM-example-2
    template: centos-7
    cluster: Default
    cpu_cores: 4
    cpu_sockets: 1
    memory: 4GiB
    nics:
      - name: nic1
        profile_name: DOC1
      - name: nic2
        profile_name: DOC2
```

## Author

Alberto Sosa (info@albertososa.es)
